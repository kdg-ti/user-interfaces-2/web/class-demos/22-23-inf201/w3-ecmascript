/*
* Wat is fluetn API ?
* */
{
    const Cat = {
        meow() {
            console.log("Miauw! Ik ben een poes.");
            return this;
        }
    };

    //methoden aan elkaar te ketenen om complexe bewerkingen uit te voeren en leesbaarder te maken.
    Cat.meow().meow().meow().meow().meow();
}

/*
* Maak een Pizza-object met een topping-methode die een bericht teruggeeft dat zegt:
* "Een pizza met {topping} als topping".
* Gebruik de fluent API om een keten van topping-methoden te maken die de topping van de pizza opbouwen.
* Voeg tot slot een bake-methode toe die het bericht "Je pizza is gebakken!" teruggeeft.
* */
{
    type Pizza = {
        bake(): Pizza;
        toppings: string[];
        topping(t: string): Pizza;
    }

    const Pizza: Pizza = {
        toppings: [],
        topping(t) {
            this.toppings.push(t);
            console.log(`Een pizza met ${t} als topping.`);
            return this;
        },
        bake() {
            console.log("Je pizza is gebakken!");
            return this;
        }
    };

    Pizza.topping("kaas").topping("ham").topping("champignons").bake();
}
/*
* Sorteren van een array
*
* -> Gebruik 'sort'
* */
{
    const dieren = ["Hond", "Kat", "Olifant", "Vogel", "Koala", "Aap", "Kangaroo", "Kikker"];

    dieren
        .sort((first, second) => first.length - second.length)
    console.log(dieren);
}


/*
* Je hebt een lijst met boeken en wilt de titel van het eerste boek vinden dat meer dan 500 pagina's heeft.
*
* -> Gebruik de methode 'find'.
* */
{
    const boeken = [
        {titel: "Harry Potter en de Steen der Wijzen", pagina: 223},
        {titel: "Inferno", pagina: 481},
        {titel: "De Da Vinci Code", pagina: 454},
        {titel: "The Lord of the Rings: The Fellowship of the Ring", pagina: 423},
        {titel: "Game of Thrones", pagina: 694},
        {titel: "The Wheel of time: the Eye of the world", pagina: 832}
    ];

    const grootBoek = boeken.find((boek) => {
        return boek.pagina > 500
    });
    // const grootBoek = boeken.find(boek => boek.pagina > 500);
    console.log(grootBoek?.titel);
}
// Maar is The Wheel of time: the Eye of the world niet groter dan 500 pagina's?
{
    const boeken = [
        {titel: "Harry Potter en de Steen der Wijzen", pagina: 223},
        {titel: "Inferno", pagina: 481},
        {titel: "De Da Vinci Code", pagina: 454},
        {titel: "The Lord of the Rings: The Fellowship of the Ring", pagina: 423},
        {titel: "Game of Thrones", pagina: 694},
        {titel: "The Wheel of time: the Eye of the world", pagina: 832}
    ];

    const groteBoeken = boeken.filter(boek => boek.pagina > 500);
    console.log(groteBoeken.map(boek => boek.titel));
}


/*
 * Je hebt een lijst met getallen en wilt de som van de even getallen in de lijst vinden.
 *
 * -> Gebruik de methode 'reduce'.
 */
{
    const getallen = [2, 4, 6, 8, 10, 11, 13, 15];

    const somVanEven = getallen
        .reduce((totaal, getal) => {
            if (getal % 2 === 0) {
                return totaal + getal;
            }
            return totaal;
        }, 0);

    console.log(somVanEven);
}

/*
 * Je wilt controleren of alle elementen in een lijst even getallen zijn.
 *
 * -> Gebruik de methode 'every'.
 */
{
    const getallen = [2, 4, 6, 8, 10];

    const alleEven = getallen.every(getal => getal % 2 === 0);

    if (alleEven) {
        console.log("Alle getallen zijn even!");
    } else {
        console.log("Niet alle getallen zijn even :(");
    }
}

/*
* Een gegeven array van getallen en we willen elk getal in de array met 2 vermenigvuldigen
*
* -> Gebruik de 'map' methode.
* */
{
    const getallen = [1, 2, 3, 4, 5];

    const vermenigvuldigMet2 = getallen.map(getal => getal * 2);

    console.log(vermenigvuldigMet2);
}

/*
* Stel je voor dat je een groepje dieren hebt en je wilt alleen de namen van de dieren weten die
*  vier letters lang zijn en eindigen op "y".
*
* -> Gebruik hiervoor de methoden 'filter', 'map' en 'forEach' om dit te doen.
* */
{
    const dieren = ["Hond", "Kat", "Olifant", "Vogel", "Koala", "Aap", "Kangaroo", "Kikker"];

    dieren
        .filter(dier => dier.length === 4 && dier.endsWith("y"))
        .map(dier => dier.toLowerCase())
        .forEach(dier => console.log(dier));
}

