/*
* Haal pokemon #6 op met fetch en Promises
* */
{
    fetch("https://pokeapi.co/api/v2/pokemon/6", {
        method: "GET",
        headers: {
            "Accept": "application/json"
        }
    })
        .then(resp => resp.json())
        .then(pokemonObject => {
            typeof alert != "undefined" ? alert(pokemonObject?.name) : console.log(pokemonObject?.name);
            console.log(pokemonObject?.sprites?.front_default);
        });
}
